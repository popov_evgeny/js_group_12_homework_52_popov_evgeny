import {Component} from '@angular/core';
import {Card, CardDeck} from "../lib/CardDeck";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'task';
  card: any;
  newCard: any;
  cards: any;
  score: any;
  scoreForUser = 0;
  win = '';
  lose = '';


  constructor() {
    this.card = new CardDeck();
    this.cards = this.card.getCards(2);
    this.addScore();
  }


  addScore() {
    this.scoreForUser = 0;
    for (let i = 0; i < this.cards.length; i++) {
      this.score = new Card(this.cards[i].rank, this.cards[i].suit);
      this.scoreForUser = this.scoreForUser + this.score.getScore();
    }
    if (this.scoreForUser === 21){
      this.win = 'You win!';
    } else if (this.scoreForUser > 21) {
      this.lose = 'You lose!';
    }
  }

  addCard() {
    this.cards.push(this.card.getCard());
    this.addScore();
  }

  reset() {
    this.win = '';
    this.lose = '';
    this.card = new CardDeck();
    this.cards = this.card.getCards(2);
    this.addScore();
  }
}


