import {Component, Input} from '@angular/core';


@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent {
  @Input() rank = '';
  @Input() suit = '';
  cardClass = '';
  symbol = '';


  getClassName(rank:string, suit:string) {
    switch (rank) {
      case '2':
      return this.cardClass = `card rank-2 ${suit}`;

      case '3':
      return this.cardClass = `card rank-3 ${suit}`;

      case '4':
      return this.cardClass = `card rank-4 ${suit}`;

      case '5':
      return this.cardClass = `card rank-6 ${suit}`;

      case '6':
      return this.cardClass = `card rank-6 ${suit}`;

      case '7':
      return this.cardClass = `card rank-7 ${suit}`;

      case '8':
      return this.cardClass = `card rank-8 ${suit}`;

      case '9':
      return this.cardClass = `card rank-9 ${suit}`;

      case '10':
      return this.cardClass = `card rank-10 ${suit}`;

      case 'J':
      return this.cardClass = `card rank-j ${suit}`;

      case 'Q':
      return this.cardClass = `card rank-q ${suit}`;

      case 'K':
      return this.cardClass = `card rank-k ${suit}`;

      case 'A':
      return this.cardClass = `card rank-a ${suit}`;

    }

    return this.cardClass;
  }

  getSymbol(suit:string) {
    switch (suit) {
      case 'diams':
        return this.symbol = '♦';

      case 'hearts':
        return this.symbol = '♥';

      case 'clubs':
        return this.symbol = '♣';

      case 'spades':
        return this.symbol = '♠';
    }

    return this.symbol;

  }
}

