
export class Card {
  score = 0;

  constructor(public rank:string, public suit:string) {
  }

  getScore() {
    switch (this.rank) {
      case 'J':
      return this.score = 10;

      case 'Q':
      return this.score = 10;

      case 'K':
      return this.score = 10;

      case 'A':
      return this.score = 11;

      default :
      return  this.score = parseInt(this.rank);
    }
  }
}

export class CardDeck {
  ranks = ['2', '3', '4', '5', '6', '7', '8', '9', '10', 'J', 'Q', 'K', 'A'];
  suit = ['diams', 'hearts', 'clubs', 'spades'];
  cardDeck: object [] = [];
  card = {};
  indexCard = 0;
  randomCard = {};
  cards: object [] = [];

  constructor() {
    for (let valueRank of this.ranks) {
      for (let valueSuit of this.suit){
        this.card = {
          rank:valueRank,
          suit: valueSuit
        }
        this.cardDeck.push(this.card)
      }
    }
  }

  getCard(){
      this.indexCard = Math.floor(Math.random() * this.cardDeck.length);
      this.randomCard = this.cardDeck[this.indexCard];
      this.cardDeck.splice(this.indexCard, 1);

    return this.randomCard;
  }

  getCards(howMany: number) {
    for (let i = 0; i < howMany; i++){
      this.cards.push(this.getCard());
    }

    return this.cards
  }
}

